/*hamburguer menu*/
const mobileBtn = document.querySelector("#btn-mobile");
const navLinksToggle = document.querySelectorAll(".menu-link a");

function toggleMenu() {
  const nav = document.querySelector(".nav-menu");
  nav.classList.toggle("active");
}
function toggleLink() {
  navLinksToggle.forEach((navLink) => {
    navLink.addEventListener("click", toggleMenu);
  });
}
mobileBtn.addEventListener("click", toggleMenu);
toggleLink();
/*hamburguer menu*/

/*sticky header*/
var stickyHeader = document.querySelector("#header");
var navLinks = document.querySelector(".menu-list");
var geometricHeader = document.querySelector("header .geometric-header");
var btnWhatsApp = document.querySelector("header .btn-whatsapp-header");

window.addEventListener("scroll", function (e) {
  if (window.pageYOffset > 100) {
    stickyHeader.style.backgroundColor = "rgba(0, 0, 0, 1)";
    geometricHeader.style.opacity = 0;
    btnWhatsApp.style.top = "calc(100% - 80px - 1rem)";
    stickyHeader.style.padding = "2rem 0";

    if (window.matchMedia("(max-width: 1000px)").matches) {
      navLinks.style.backgroundColor = "rgba(0, 0, 0, 1)";
      navLinks.style.top = "9rem";
      stickyHeader.style.padding = "5rem 0";
    }
  } else {
    header.style.backgroundColor = "rgba(0, 0, 0, 0)";
    geometricHeader.style.opacity = 1;
    btnWhatsApp.style.top = "5rem";
    // stickyHeader.style.padding = "4rem 0";

    if (window.matchMedia("(max-width: 1000px)").matches) {
      navLinks.style.backgroundColor = "rgba(0, 0, 0, 1)";
      navLinks.style.top = "14rem";
    }
  }
});
/*sticky header*/
