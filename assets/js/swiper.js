var swiperHeader = new Swiper(".swiperHeader", {
  navigation: {
    nextEl: ".swiper-button-next-header",
    prevEl: ".swiper-button-prev-header",
  },
  loop: true,
});

var swiperParceiros = new Swiper(".swiperParceiros", {
  slidesPerView: 1,
  loop: true,
  spaceBetween: 30,
  pagination: {
    el: ".swiper-pagination-parceiros",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next-parceiros",
    prevEl: ".swiper-button-prev-parceiros",
  },
});

var swiperTestimonial = new Swiper(".swiperTestimonial", {
  slidesPerView: 2,
  loop: true,
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  breakpoints: {
    0: {
      slidesPerView: 1,
      spaceBetween: 40,
      slidesPerGroup: 1,
    },
    1000: {
      slidesPerView: 2,
      spaceBetween: 40,
      slidesPerGroup: 1,
    },
  },
});
