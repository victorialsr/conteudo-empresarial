<footer>
      <div class="container">
         <div class="row-1">
            <div class="col-1">
               <h2>Como podemos ajudar?</h2>
               <p>
                  <span>Fale com um especialista agora mesmo!</span>
               </p>
               <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
               <a href=#" class="btn --main">Fale com um especialista</a>
            </div>
            <div class="col-2">
               <div class="contact-1">
                  <a href="https://api.whatsapp.com/send?phone=5511000000000">
                  <img src="<?php bloginfo('template_url');?>/assets/img/icon-telephone.png" alt="Ícone de telefone branco">
                  <p>(11) 0000-0000</p>
                  </a>
                  <a href="https://api.whatsapp.com/send?phone=5513000000000">
                     <img src="<?php bloginfo('template_url');?>/assets/img/icon-telephone.png" alt="Ícone de telefone branco">
                     <p>(13) 0000-0000</p>
                  </a>
                  <a href="mailto:contato@conteudoempresarial.com.br">
                     <img src="<?php bloginfo('template_url');?>/assets/img/icon-email.png" alt="Ícone de carta branca">
                     <p>contato@conteudoempresarial.com.br</p>
                  </a>
               </div>
               <div class="contact-2">
                  <a href="https://br.linkedin.com/company/conteudoempresarial">
                     <div class="icon-footer">
                        <img src="<?php bloginfo('template_url');?>/assets/img/icon-linkedin.svg" alt="Logo da empresa Linkedin">
                     </div>
                  </a>
                  <a href="https://www.flickr.com/photos/conteudoempresarial/">
                     <div class="icon-footer">
                        <img src="<?php bloginfo('template_url');?>/assets/img/icon-flickr.svg" alt="Logo da empresa Flickr">
                     </div>
                  </a>
                  
                  <a href="#" class="btn --portfolio">Nosso portfólio
                  </a>
               </div>
            </div>
         </div>
      </div>
      <section class="section-copyright-footer">
         <div class="container">
            <div class="row-2 --footer">
               <a href="https://www.conteudoempresarial.com.br/">
                  <img src="<?php bloginfo('template_url');?>/assets/img/logo-conteudo-empresarial.png" alt="Logo da empresa Conteúdo Empresarial">
               </a>
               <p>Conteúdo Empresarial © 2020 - Todos os direitos reservados </p>
               <div class="kbr-footer">
                  <a href="https://www.kbrtec.com.br/"> 
                     <p>Desenvolvido por</p>
                     <img src="<?php bloginfo('template_url');?>/assets/img/logo-kbrtec.svg" alt="Logo da empresa KBR TEC">
                  </a>
               </div>
            </div>
         </div>
      </section>
   </footer>
   <?php wp_footer(); ?>
   <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
   <script src="<?php bloginfo('template_url');?>/assets/js/script.js"></script>
   <script src="<?php bloginfo('template_url');?>/assets/js/swiper.js"></script>
</body>
</html>