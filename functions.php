<?php
function theme_setup(){
/** post thumbnail **/
add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'theme_setup' );
function my_pagination(){
   echo paginate_links(array(
    'prev_text' => '',
    'next_text' => ''
));
}

add_action( 'init' , 'create_service_post_type' );
function create_service_post_type(){
	register_post_type('servicos',
        array(
            'labels' => array(
            'name' => _('Serviços'),
            'singular_name' => _('Serviço')
        ),
        'public' => true,
        'has_archive' => true,
        )
);		
}

add_action( 'init' , 'create_partners_post_type' );
function create_partners_post_type(){
	register_post_type('parceiros',
        array(
            'labels' => array(
            'name' => _('Parceiros'),
            'singular_name' => _('Parceiro')
        ),
        'public' => true,
        'has_archive' => true,
        )
);		
}

add_action( 'init' , 'create_about_us_highlights_post_type' );
function create_about_us_highlights_post_type(){
	register_post_type('quem-somos-destaques',
        array(
            'labels' => array(
            'name' => _('Quem Somos - Destaques'),
            'singular_name' => _('Quem Somos - Destaque')
        ),
        'public' => true,
        'has_archive' => true,
        )
);		
}
?>