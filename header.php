<!DOCTYPE html>
<html lang="pt-br">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php if ($_SESSION['page'] == ""){?>
		<title><?php the_title(); ?> | Conteúdo Empresarial</title>
		
	<?php } else { ?> 
		<title><?php echo $_SESSION['page']; ?> | Conteúdo Empresarial</title>
	<?php } ?>
	<link rel="preconnect" href="https://fonts.googleapis.com" />
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;300;400;500;600;700;900&display=swap" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url');?>/assets/css/style.css">
	<?php wp_head(); ?>
</head>

<body>
	<header id="header">
	<div class="logo">
		<a href="<?php echo get_site_url()?>/" class="a-breadcrumb">
				<img src="<?php bloginfo('template_url');?>/assets/img/logo-conteudo-empresarial-header.png" alt="Logo da empresa Conteúdo Empresarial" class="logo-header">
			</a>
		</div>
		<div class="menu">
			<nav class="nav-menu">
				<button id="btn-mobile">
					<span id="hamburguer"></span>
				</button>
				<ul class="menu-list">
				<?php if(is_page('Home')) { ?>
						<li class="menu-link"><a href="#quem-somos">Quem Somos</a></li>
						<li class="menu-link"><a href="#lideranca">Liderança</a></li>
						<li class="menu-link"><a href="#servicos">O que fazemos</a></li>
						<li class="menu-link"><a href="#parceiros">Quem trabalha com a gente</a></li>
						<li class="menu-link"><a href="#depoimentos">O que dizem do nosso trabalho</a></li>
					<?php } else { ?>
						<li class="menu-link"><a href="<?php echo bloginfo('url'); ?>/home#quem-somos">Quem Somos</a></li>
						<li class="menu-link"><a href="<?php echo bloginfo('url'); ?>/home#lideranca">Liderança</a></li>
						<li class="menu-link"><a href="<?php echo bloginfo('url'); ?>/home#servicos">O que fazemos</a></li>
						<li class="menu-link"><a href="<?php echo bloginfo('url'); ?>/home#parceiros">Quem trabalha com a gente</a></li>
						<li class="menu-link"><a href="<?php echo bloginfo('url'); ?>/home#depoimentos">O que dizem do nosso trabalho</a></li>
					<?php } ?>
					<li class="menu-link"><a href="<?php echo bloginfo('url'); ?>/blog">Blog</li></a>
					
				</ul>
			</nav>   
		</div>	
		<img src="<?php bloginfo('template_url');?>/assets/img/geometric-header.png" alt="Desenho geométrico com degradê verde" class="geometric-header">
		<a href="https://api.whatsapp.com/send?phone=5513000000000" class="btn-whatsapp-header">
			<img src="<?php bloginfo('template_url');?>/assets/img/icon-whatsapp-header.svg" alt="Logo do aplicativo de mensagens Whatsapp" class="icon-whatsapp-header">
		</a>
	</header>