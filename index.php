<?php
/* Template Name: Blog */ 
if( isset($_SESSION) ){ session_start(); }
$_SESSION['page'] = "Blog";
get_header();
?>

<main>
    <section class="section-hero-blog">
        <div class="line"> </div>
        <img src="<?php bloginfo('template_url');?>/assets/img/geometric-header-blog.svg" alt="Desenho geométrico verde" class="geometric-blog">
        <img src="<?php bloginfo('template_url');?>/assets/img/logo-vida-de-jornalista.svg" alt="Logo do blog Vida de Jornalista" class="logo-blog">
        <div class="container --blog">
            <nav class="breadcrumb">
                <ul>
                    <li><a href="<?php echo get_site_url()?>/" class="a-breadcrumb">Home</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/blog" class="a-breadcrumb-active">Blog</a></li>
                </ul>
            </nav>
        </div>
    </section>
    <section class="section-intro-blog">
        <div class="container">
            <div class="row">
                <div class="img"><img src="<?php bloginfo('template_url');?>/assets/img/erica-amores-blog.png" alt="Imagem de Érica amores" class="img-round"></div>
                <div class="content">
                    <h3>Bem vindos ao Blog Vida de Jornalista.</h3>
                    <p>Sou Érica Amores e este é o "Vida de Jornalista"! Minha missão é ajudar estudantes, profissionais e empreendedores das áreas de comunicação e marketing a superar seus desafios pessoais: para ter sucesso no primeiro emprego, para escalar a carreira ou fazer crescer o seu próprio negócio.</p>
                </div>
            </div>
        </div>

    </section>
    <section class="section-blog">
        <div class="container">
            <div class="search-filter">
                <h2>Últimas do blog</h2>
                <div class="search-filter-inputs">
                    <?php get_template_part('search-filter'); ?>
                </div>
            </div>
            <div class="cards-blog">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <div class="card-blog">
                    <div class="image-blog">
                    <?php if ( has_post_thumbnail( $post->ID ) ): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );?>
								<img src="<?php echo $image[0]; ?>" alt="" class="img-blog">
							<?php else: ?>
								<img src="http://via.placeholder.com/1000x300" alt="" class="">
							<?php endif; ?> 
                    </div>
                    <div class="content-blog">
                        <p class="date-blog"><?php echo get_the_date('d/m/Y'); ?></p>
                        <a href="#"><p class="category-blog"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></p></a>
                        
                        <h2 class="title-blog"><?php echo the_title(); ?></h2>
                        <p class="excerpt-blog"><?php echo get_the_excerpt(); ?></p>        
                        <a class="anchor-blog" href="<?php echo the_permalink(); ?>" >Leia mais</a>
                    </div>
                </div>
<?php endwhile;?>
            </div>   
            <div class="pagination">
                <?php echo my_pagination();?>
            </div> 
        </div>
    </section>
<?php else: ?>
    <section>
        <div class="container">
            <h1>Página não encontrada</h1>
        </div>
    </section>
<?php endif; ?>
</main>

<?php 
get_footer();
?>