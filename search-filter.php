<form role="search" method="get" id="searchform" class="searchform" action="<?php echo bloginfo('url'); ?>/blog">		
        <?php wp_dropdown_categories(array(
            'show_option_all' => 'Todas as categorias',
            'orderby' => 'name',
            'echo' => 1,
            'selected' => $cat,
            'hierarchical' => true,
            'class' => 'cat_dropdown',
            'id' => 'custom-cat-drop',
            'value_field' => 'term_id',
        ));?>
        <div class="search-filter-text">
            <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="O que você procura?"/>
		    <button type="submit" id="searchsubmit" >
                <img src="<?php bloginfo('template_url');?>/assets/img/icon-search.svg" alt="Ícone de lupa na cor laranja">
            </button>
        </div>
</form>      