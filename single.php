<?php
if( isset($_SESSION) ){ session_start(); }
$_SESSION['page'] = '';
get_header();
?>
<main>
    <section class="section-hero-blog --single"style='background-image: url("<?php the_post_thumbnail_url()?>")'>
        <div class="line"> </div>
        <div class="container --blog">
            <nav class="breadcrumb">
                <ul>
                <li><a href="<?php echo get_site_url()?>/" class="a-breadcrumb">Home</a></li>
                    <li><a href="<?php echo bloginfo('url'); ?>/blog" class="a-breadcrumb-active">Blog</a></li>
                </ul>
            </nav>
            <div class="right-line">
        </div>
    </section>
    <section class="section-blog --single">
        <div class="container">
            <div class="content-blog --single">
                <p class="date-blog --single"><?php echo get_the_date('d/m/Y'); ?></p>
                    <a href="http://localhost/wordpress/category/estudante/"><p class="category-blog --single"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></p></a>
                    <h2 class="title-blog --single"><?php echo the_title(); ?></h2>
                    <div class="text-blog">
                        <?php echo get_the_content();?> 
                    </div>
                    <div class="content-blog-share">
                        <a class="anchor-blog --single" href="<?php echo get_site_url()?>/blog" >Voltar</a>
                        <a href="http://www.facebook.com/sharer/sharer.php?u=<?= get_permalink() ?>"> 
                            <img src="<?php bloginfo('template_url');?>/assets/img/icon-facebook-share.svg" alt="Ícone da rede social Facebook">
                        </a>
                        <a href="https://wa.me/?text=<?= get_permalink() ?>"> 
                            <img src="<?php bloginfo('template_url');?>/assets/img/icon-instagram-share.svg" alt="Ícone da rede social Whatsapp">
                        </a>
                        <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?= get_permalink(); ?>">
                            <img src="<?php bloginfo('template_url');?>/assets/img/icon-linkedin-share.svg" alt="Ícone da rede social Linkedin">
                        </a>                  
                        <p>Compartilhe</p>
                    </div>
            </div>
        </div>
    </section>
    <section class="section-newsletter-blog">
        <div class="container">
            <div class="row">
                <img src="<?php bloginfo('template_url');?>/assets/img/icon-email-newsletter.svg" alt="Ícone de carta na cor branca">
                <div class="text-newsletter">
                    <h3>Fique por dentro de tudo</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum. </p>
                </div>
            </div>
            <form action="" method="post">
                <input type="text" placeholder="Nome:" name= "nome">
                <input type="email" name="email" id="email" placeholder="E-mail: ">
                <button type="submit">Enviar</button>
            </form>
        </div>
    </section>
    <section class="section-more-posts --single">
        <div class="container">
            <h2 class="title">Mais de Érica Amores</h2>
        <div class="row">
    <?php
                $arg_relacionadas = array(
                  'posts_per_page' => 2,
                  'post__not_in' => array( get_the_ID() ),
                  'orderby' => 'rand'
                );
    $relacionadas = new WP_Query( $arg_relacionadas );

    if( $relacionadas->have_posts() ) :
        while( $relacionadas->have_posts() ):
            $relacionadas->the_post();
    ?>
    <div class="card-blog --single --morePosts">
                    <div class="image-blog --morePosts">
                    <?php if ( has_post_thumbnail( $post->ID ) ): ?>
							<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );?>
								<img src="<?php echo $image[0]; ?>" alt="" class="img-blog --morePosts">
							<?php else: ?>
								<img src="http://via.placeholder.com/1000x300" alt="" class="">
							<?php endif; ?> 
                    </div>
                    <div class="content-blog --morePosts">
                        <p class="date-blog"><?php echo get_the_date('d/m/Y'); ?></p>
                        <a href="http://localhost/wordpress/category/estudante/"><p class="category-blog"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></p></a>
                        
                        <h2 class="title-blog --morePosts"><?php echo the_title(); ?></h2>
                        <p class="excerpt-blog"><?php echo get_the_excerpt(); ?></p>        
                        <a class="anchor-blog" href="<?php echo the_permalink(); ?>" >Leia mais</a>
                    </div>
                </div>
    <?php endwhile; ?>
        <?php endif; wp_reset_postdata();
    ?></div></div>
    </section>
</main>
<?php 
get_footer();
?>