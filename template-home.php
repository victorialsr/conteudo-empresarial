<?php 
/*
Template Name: Home
*/
// ini_set('session.save_path', getcwd() . '/tmp');
if( isset($_SESSION) ){ session_start(); }
$_SESSION['page'] = '';
get_header();
$argsServices = array(
   'post_type' => 'servicos',
   'posts_per_page' => 8
);
$the_query_services = new WP_query($argsServices);

$argsPartners = array(
   'post_type' => 'parceiros'
);
$the_query_partners = new WP_query($argsPartners);

$argsAboutUsHighlights = array(
   'post_type' => 'quem-somos-destaques',
   'posts_per_page' => 4
);
$the_query_about_us_highlights = new WP_query($argsAboutUsHighlights);
?>
   <main>
      <section class="section-hero">
         <div class="left-line --hero">Conteúdo empresarial</div>
            <div class="swiper swiperHeader">
               <div class="swiper-wrapper">
                  <div class="swiper-slide">
                     <div class="header-texto">
                        <h1>Somos a agência que quer trabalhar <span>com você!</span></h1>
                        <a href="<?php the_field('link_botao_cta');?>" class="btn --main --hero"><?php the_field('texto_botao_cta');?></a>
                     </div>
                     <div class="header-afiliados">
                        <a href="https://www.afiliadosbrasil.com.br">
                           <img src="<?php bloginfo('template_url');?>/assets/img/logo-afiliados.png" alt="Logo da empresa Afiliados Brasil">
                        </a>
                        <div class="linha-header"></div>
                        <div class="afiliados-texto">
                           <p class="paragraph-afiliados">O afiliados Brasil é nosso cliente desde 2013</p>
                           <p class="paragraph-afiliados">Jobs: assessoria de empresa, fotojornalismo, conteúdo e mídias sociais</p>
                        </div>
                     </div>
                  </div>
                  <div class="swiper-slide">
                     <div class="header-texto">
                        <h1>Somos a agência que quer trabalhar <span>com você!</span></h1>
                        <a href="<?php the_field('link_botao_cta');?>" class="btn --main"><?php the_field('texto_botao_cta');?></a>
                     </div>
                     <div class="header-afiliados">
                        <a href="https://www.afiliadosbrasil.com.br">
                           <img src="<?php bloginfo('template_url');?>/assets/img/logo-afiliados.png" alt="Logo da empresa Afiliados Brasil">
                        </a>
                        <div class="linha-header"></div>
                        <div class="afiliados-texto">
                           <p class="paragraph-afiliados">O afiliados Brasil é nosso cliente desde 2013</p>
                           <p class="paragraph-afiliados">Jobs: assessoria de empresa, fotojornalismo, conteúdo e mídias sociais</p>
                        </div>
                     </div>
                  </div>
                  <div class="swiper-slide">
                     <div class="header-texto">
                        <h1>Somos a agência que quer trabalhar <span>com você!</span></h1>
                        <a href="<?php the_field('link_botao_cta');?>" class="btn --main"><?php the_field('texto_botao_cta');?></a>
                     </div>
                     <div class="header-afiliados">
                        <a href="https://www.afiliadosbrasil.com.br">
                           <img src="<?php bloginfo('template_url');?>/assets/img/logo-afiliados.png" alt="Logo da empresa Afiliados Brasil">
                        </a>
                        <div class="linha-header"></div>
                        <div class="afiliados-texto">
                           <p class="paragraph-afiliados">O afiliados Brasil é nosso cliente desde 2013</p>
                           <p class="paragraph-afiliados">Jobs: assessoria de empresa, fotojornalismo, conteúdo e mídias sociais</p>                   
                        </div>
                     </div>
                  </div>
               </div>
               <div class="swiper-buttons-container">
                  <div class="swiper-button-prev swiper-button-prev-header"></div>
                  <div class="swiper-button-next swiper-button-next-header"></div>
               </div>
            </div>
         <div class="right-line"> <img class="mouse-icon" src="<?php bloginfo('template_url');?>/assets/img/mouse.png" alt="Ícone de mouse laranja"></div>
      </section>

      <section class="section-quem-somos" id="quem-somos">
         <div class="container">
            <div class="row-1 --quemSomos">
               <div class="col-1 --quemSomos">
                  <h2 class="title --quemSomos"><?php the_field('titulo_quem_somos'); ?></h2>
                  <p class="paragraph --quemSomos">
                  <?php the_field('descricao_quem_somos_principal_1', 6); ?>
                  <span><?php the_field('descricao_enfase_quem_somos_principal_1', 6); ?></span></p>
                  <a href="<?php the_field('link_botao_cta');?>" class="btn --main">
                  <?php the_field('texto_botao_cta');?>
                  </a>
               </div>
               <div class="col-2 --quemSomos">
                  <p class="paragraph --quemSomos">
                    <?php the_field('descricao_quem_somos_principal_2', 6); ?>
                  </p>
                  <p class="paragraph --quemSomos">
                  <?php the_field('descricao_quem_somos_principal_3', 6); ?>
                  </p>
                  <p class="paragraph --quemSomos"></span>
                  <?php the_field('descricao_quem_somos_principal_4', 6); ?><span><?php the_field('descricao_enfase_quem_somos_principal_2', 6); ?>
                  </p>
               </div>
            </div>
            <div class="row-2 row-grid --quemSomos">
               <?php if($the_query_about_us_highlights -> have_posts() ):?>
               <?php while ($the_query_about_us_highlights->have_posts() ) : $the_query_about_us_highlights->the_post(); ?>
               <div class="box --quemSomos">
                  <img src="<?php the_field('icone_quem_somos_destaque')?>">
                  <p><span><?php the_field('descricao_enfase_quem_somos_destaque'); ?></span><?php the_field('descricao_quem_somos_destaque'); ?></p>
               </div>
               <?php endwhile; ?>
               <?php endif; wp_reset_postdata(); ?>
            </div>
         </div>
      </section>

      <section class="section-lideranca" id="lideranca">
         <div class="row --lideranca">
            <div class="col-1">
            </div>
            <div class="col-2">
               <h2>Liderança</h2>
               <div class="retangulo-verde"></div>
               <p class="destaque">Olá!</p>
               <p>Sou Érica Amores</p>
               <div class="referencia">
                  <img src="<?php bloginfo('template_url');?>/assets/img/icon-checkmark.svg" alt="Ícone de sinal de verificação na cor verde">
                  <p>Jornalista (com diploma)</p>
               </div>
               <div class="referencia">
                  <img src="<?php bloginfo('template_url');?>/assets/img/icon-checkmark.svg" alt="Ícone de sinal de verificação na cor verde">
                  <p>Marketeira (por escolha) </p>
               </div>
               <div class="referencia">
                  <img src="<?php bloginfo('template_url');?>/assets/img/icon-checkmark.svg" alt="Ícone de sinal de verificação na cor verde">
                  <p>Copywriter em formação (por aspiração)</p>
               </div>
               <div class="referencia">
                  <img src="<?php bloginfo('template_url');?>/assets/img/icon-checkmark.svg" alt="Ícone de sinal de verificação na cor verde">
                  <p>Empreenderora há mais de 15 anos</p>
               </div>
               <p class="paragraph-lideranca">Desde 2009, estou dedicada à Conteúdo Empresarial, minha segunda empreitada empresarial, e focada em conquistar resultados significativos para os nossos clientes.</p>
               <div class="referencia-linkedin">
                  <p class="paragraph-lideranca-cta">Quer saber mais sobre a minha jornada?
                     <a href="https://br.linkedin.com/in/ericaamores" class="a-linkedin">Clique aqui!
                        <img src="<?php bloginfo('template_url');?>/assets/img/logo-linkedin.svg" alt="Ícone de sinal de verificação na cor verde">
                     </a>
                  </p> 
               </div>
            </div>
            <div class="col-3">
            <div class="left-line --lideranca">Liderança</div>
            </div>
         </div>
      </section>

      <section class="section-servicos" id="servicos">
         <div class="container --servicos">
            <h3 class="subtitle"><?php the_field('subtitulo_servicos'); ?></h3>
            <h2 class="title"><?php the_field('titulo_servicos'); ?></h2>
         <div class="row-2 row-grid --servicos">
            <?php if($the_query_services -> have_posts() ):?>
               <?php while ($the_query_services->have_posts() ) : $the_query_services->the_post(); ?>
               <div class="box --servicos">
                  <img src="<?php the_field('icone_do_servico');?>">
                  <div class="teste-div">
                     <h2><?php the_field('titulo_do_servico'); ?></h2>
                  </div>
                  <p>
                  <?php the_field('descricao_do_servico'); ?>
                  </p>
               </div>
            <?php endwhile; ?>
            <?php endif; wp_reset_postdata(); ?>
               <div class="box --servicos">
                  <div class="box-content">
                     <img src="<?php bloginfo('template_url');?>/assets/img/icon-servicos-cta.svg" alt="Ícone que contém pessoas conversando na cor verde e um telefone na cor laranja">
                     <p><strong>Fale agora</strong> com um de nossos <strong>especialistas</strong></p>
                  </div>
                  <a href="<?php the_field('link_botao_cta');?>" class="btn --main"><?php the_field('texto_botao_cta');?></a>
               </div>
            </div>
         
         </div>
      </section>

      <section class="section-parceiros" id="parceiros">
         <div class="container">
            <div class="row">
               <div class="col-1">
                  <img src="<?php bloginfo('template_url');?>/assets/img/geometric.png" class="geometric"alt="Desenho geométrico verde">
                  <h3 class="subtitle --parceiros"><?php the_field('subtitulo_parceiros'); ?></h3>
                  <h2 class="title --parceiros"><?php the_field('titulo_parceiros'); ?></h2>
                  <p><?php the_field('descricao_parceiros', 6); ?></p>
               </div>
               <div class="col-2">
                  <!-- Slider main container -->
                  <div class="swiper swiperParceiros">
                     <div class="swiper-wrapper">
                     <?php if($the_query_partners -> have_posts() ):?>
                     <?php while ($the_query_partners->have_posts() ) : $the_query_partners->the_post(); ?>
                        <div class="swiper-slide">
                           <div class="items-slider">
                              <a href="<?php the_field('link_parceiros_slider_1');?>">
                                 <div class="item">
                                    <img src="<?php the_field('imagem_parceiros_slider_1');?>"> 
                                 </div>
                              </a>
                              <a href="<?php the_field('link_parceiros_slider_2');?>">
                                 <div class="item">
                                    <img src="<?php the_field('imagem_parceiros_slider_2');?>">
                                 </div>
                              </a>
                              <a href="<?php the_field('link_parceiros_slider_3');?>">
                                 <div class="item">
                                    <img src="<?php the_field('imagem_parceiros_slider_3');?>">
                                 </div>
                              </a>
                              <a href="<?php the_field('link_parceiros_slider_4');?>">
                                 <div class="item">
                                    <img src="<?php the_field('imagem_parceiros_slider_4');?>">
                                 </div>
                              </a>
                              <a href="<?php the_field('link_parceiros_slider_5');?>">
                                 <div class="item">
                                    <img src="<?php the_field('imagem_parceiros_slider_5');?>">
                                 </div>
                              </a>
                           </div>
                        </div>
                     <?php endwhile; ?>
                     <?php endif; wp_reset_postdata(); ?>
                     </div>
                     <div class="pagination-container">
                        <div class="swiper-button-prev-parceiros"></div>
                        <div class="swiper-pagination-parceiros"></div>
                        <div class="swiper-button-next-parceiros"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <section class="section-depoimentos" id="depoimentos">
         <div class="container">
            <h3 class="subtitle"><?php the_field('subtitulo_depoimentos'); ?></h3>
            <h2 class="title"><?php the_field('titulo_depoimentos'); ?></h2>
               <div class="swiper swiperTestimonial">
                  <div class="swiper-wrapper">
                     <div class="swiper-slide">
                        <div class="card-depoimento">
                           <div class="card-depoimento-content">
                              <img src="<?php bloginfo('template_url');?>/assets/img/icon-quotationmark.svg" alt="Ícone de símbolo de citação na cor laranja" class="icon-quotationmarks">
                              <p><?php the_field('texto_depoimento', 6); ?></p>
                              <div class="card-depoimento-nome depoimento-1">
                                 <img src="<?php the_field('foto_autor_depoimento', 6); ?>" alt="Imagem do designer Marcelo Ribeiro" class="icon-depoimento-borda">
                                 <p><span><?php the_field('nome_do_autor_depoimento', 6); ?> - <?php the_field('profissao_do_autor_depoimento', 6); ?></span></p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="swiper-slide"> 
                        <div class="card-depoimento">
                           <div class="card-depoimento-content">
                              <img src="<?php bloginfo('template_url');?>/assets/img/icon-quotationmark.svg" alt="Ícone de símbolo de citação na cor laranja" class="icon-quotationmarks">
                              <p><?php the_field('texto_depoimento_2', 6); ?></p>
                              <div class="card-depoimento-nome depoimento-2">
                                 <p><span><?php the_field('nome_do_autor_depoimento_2', 6); ?> - <?php the_field('profissao_do_autor_depoimento_2', 6); ?></span></p>
                                 <img src="<?php the_field('foto_autor_depoimento_2', 6); ?>" alt="Imagem do designer Pablo Soares" class="icon-depoimento-borda">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="swiper-buttons-container">
                     <div class="swiper-button-prev"></div> 
                     <div class="swiper-button-next"></div> 
                  </div>
               </div>
         </div>
      </section>
   </main>

<?php get_footer();?>

